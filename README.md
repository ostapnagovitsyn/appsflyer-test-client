##appsflyer-test-client

this app is based on create-react-app + react apollo client + Material UI

to run
- create `.env` file and fill it with env variables from `.env.example`
- run `yarn start`

####todo list
- [x] initial project structure
- [ ] add .env files
- [ ] add real calls to server
- [ ] implement app logic
- [ ] improve layout
- [ ] Docker
- [ ] gitlab integration
