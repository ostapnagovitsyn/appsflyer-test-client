import gql from "graphql-tag";
import * as React from "react";
import { Query } from "react-apollo";
import { GetCommitQuery, GetCommitQueryVariables } from "./__generated__/types";
import { GetCommit as QUERY } from "./queries";

class CommitQuery extends Query<GetCommitQuery, GetCommitQueryVariables> {}

export interface ICommitProps {
  sha: "sadasffsdf"
}

const GET_COMMITS = gql`
    query commits ($page: Int) {
        commits(page: $page) {
            id
        }
    }
`;

export const Character: React.SFC<ICommitProps> = props => {
  const { sha } = props;

  return (
    <CommitQuery query={QUERY} variables={{ sha }}>
      {({ loading, data, error }) => {
        if (loading) return <div>Loading</div>;
        if (error) return <h1>ERROR</h1>;
        if (!data) return <div>no data</div>;

        const { hero } = data;
        return (
          <div>
            {hero && (
              <div>
                <h3>{hero.name}</h3>
                {hero.friends &&
                hero.friends.map(
                  friend =>
                    friend && (
                      <h6 key={friend.id}>
                        {friend.name}:{" "}
                        {friend.appearsIn.map(x => x && x.toLowerCase()).join(", ")}
                      </h6>
                    )
                )}
              </div>
            )}
          </div>
        );
      }}
    </CommitQuery>
  );
};

export default Character;
