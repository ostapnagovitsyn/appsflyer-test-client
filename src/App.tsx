import AppBar from "@material-ui/core/AppBar";
import Button from "@material-ui/core/Button";
import Grid, { GridSpacing } from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Paper from "@material-ui/core/Paper";
import { Theme, withStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import MenuIcon from "@material-ui/icons/Menu";
import withRoot from "./withRoot";

import * as React from "react";
import "./App.css";

const styles = (theme: Theme) => ({
  root: {
    paddingTop: theme.spacing.unit * 20,
    textAlign: "center"
  }
});

interface IProps {
  classes: { [key: string]: any }
}

interface IState {
  project: string
}

class App extends React.Component<IProps, IState> {
  public state: IState;

  constructor (props: Readonly<IProps>) {
    super(props);
    this.state = {
      project: 'ostapnagovitsyn/appsflyer-test-client'
    };
  }

  public handleChange = (event: React.ChangeEvent) => {
    this.setState({
      // @ts-ignore
      project: event.target.value,
    });
  };

  public render () {
    const { classes } = this.props;
    const { project } = this.state;
    const commits = [
      { id: 1, name: "first" },
      { id: 2, name: "second" }
    ];

    const spacing: GridSpacing = 16;

    return (
      <Grid container={true} className={classes.root} spacing={16}>
        <AppBar>
          <Toolbar>
            <IconButton className={classes.menuButton} color="inherit" aria-label="Menu">
              <MenuIcon/>
            </IconButton>
            <Typography variant="title" color="inherit" className={classes.flex}>
              News
            </Typography>
            <Button color="inherit">Login</Button>
          </Toolbar>
        </AppBar>
        <Grid item={true} xs={12}>
          <Grid container={true} className={classes.demo} justify="center" spacing={spacing}>
            <Grid item={true}>
              <Paper className={classes.paper}>
                <form className={classes.container} noValidate={true} autoComplete="off">
                  <TextField
                    id="name"
                    label="Name"
                    className={classes.textField}
                    value={project}
                    onChange={this.handleChange}
                    margin="normal"
                  />
                </form>
              </Paper>
            </Grid>
            <Grid item={true}>
              <Paper className={classes.paper}>
                <List component="nav">
                  {commits.map((commit, i) => {
                    return <ListItem key={i}>
                      <ListItemText primary={commit.name}/>
                    </ListItem>;
                  })}
                </List>
              </Paper>
            </Grid>
            <Grid item={true}>
              <Paper className={classes.paper}>
                commit description
              </Paper>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    );
  }
}

// @ts-ignore todo check what's going on with styles types
export default withRoot(withStyles(styles)(App));
